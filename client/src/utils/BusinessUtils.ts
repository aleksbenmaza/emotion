import {Model, VehicleType} from "../model/input/Model";

export class BusinessUtils {

  private constructor() {}

  static isCar(model: Model): boolean {
    return model.vehicleType == VehicleType[VehicleType.CAR.toString()];
  }
}
