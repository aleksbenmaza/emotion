import {Type} from "../model/input/User";
import {Token} from "../model/input/Token";

export class SecurityUtils {

  static isGranted(
    token: Token,
    type: Type
  ): boolean {
    return token != null && token.user.__type__ == type;
  }
}
