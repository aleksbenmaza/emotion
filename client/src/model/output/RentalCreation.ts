export interface RentalCreation {
  vehicleId: number
  customerId: number
  pickupDate: Date
  duration: number
  usingLoyaltyPoints: boolean
}
