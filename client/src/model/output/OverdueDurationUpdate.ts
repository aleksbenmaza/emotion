import {RentalUpdate, Type} from "./RentalUpdate";

export interface OverdueDurationUpdate extends RentalUpdate {

  overdueDuration: number
}

export function create(overdueDuration: number): OverdueDurationUpdate {
  return {
    __type__: Type.OVERDUE_DURATION,
    overdueDuration: overdueDuration
  };
}
