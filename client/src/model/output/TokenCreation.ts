export interface TokenCreation {
  emailAddress: string
  password: string
  persistent?: boolean
}
