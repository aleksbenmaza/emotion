import {VehicleType} from "../input/Model";

export interface VehicleSearch {
  modelVehicleType: VehicleType;

  modelId: number;

  modelMakeId: number;

  rentalPickupDate: Date;

  rentalNumberOfDay: number;
}
