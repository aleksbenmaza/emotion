import {Type, VehicleUpdate} from "./VehicleUpdate";

export interface HiddenUpdate extends VehicleUpdate {}

export function create(): HiddenUpdate {
  return {
    __type__: Type.HIDDEN,
  };
}
