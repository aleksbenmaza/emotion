import {Component, ViewChild} from '@angular/core';
import {Token} from "../../model/input/Token";
import {NavbarComponent} from "./page/navbar/navbar.component";
import {TokenHolder} from "../helper/token-holder.service";
import {timer} from "rxjs";
import {UnauthorizedAccessComponent} from "./page/unauthorized-access/unauthorized-access.component";
import {Router} from "@angular/router";
import {RouteUtils} from "../../utils/RouteUtils";
import {TranslateService} from "@ngx-translate/core";

const DEFAULT_LANG = 'fr';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent {
  title = 'app';

  @ViewChild(NavbarComponent)
  navbarComponent: NavbarComponent;

  unauthorized: boolean;

  constructor(
    private readonly tokenHolder: TokenHolder,
    private readonly router: Router,
    private readonly translateService: TranslateService
  ) {
    this.translateService.setDefaultLang(DEFAULT_LANG);
    this.translateService.use(DEFAULT_LANG);
  }

  tokenCreated(token: Token): void {
    this.tokenHolder.token = token;

    if(token)
      timer(
        token.expirationTime.seconds * 1000 -
        Date.now()
      ).subscribe(
        () => this.tokenHolder.token = null
      );
  }

  tokenDestroyed(): void {
    this.tokenHolder.token = null;
    RouteUtils.refresh(this.router);
  }

  get token(): Token {
    return this.tokenHolder.token;
  }

  componentAdded(component: any): void {
    if(this.unauthorized = component instanceof UnauthorizedAccessComponent)
      component.eventEmitter.subscribe(token => this.tokenCreated(token));
  }
}
