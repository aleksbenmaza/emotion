import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Model} from "../../../../../model/input/Model";
import {Router} from "@angular/router";
import {ModelAutocompleteComponent} from "../../../common/input/model-autocomplete/model-autocomplete.component";
import {VehicleCreation} from "../../../../../model/output/VehicleCreation";

@Component({
  selector: 'app-new-vehicle',
  templateUrl: './new-vehicle.component.html',
  styleUrls: ['./new-vehicle.component.css']
})
export class NewVehicleComponent implements OnInit {

  maxDate: Date;

  @Input()
  vehicleCreation: VehicleCreation;

  @ViewChild(ModelAutocompleteComponent)
  modelAutocomplete: ModelAutocompleteComponent;

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly router: Router
  ) {

    this.maxDate = new Date(Date.now());

    this.vehicleCreation = {
      modelId: null,
      serialNumber: null,
      plateNumber: null,
      numberOfKilometers: null,
      purchaseDate: null,
      purchasePrice: null,
      rentalPrice: null,
      loyaltyPoints: null,
    };
  }

  ngOnInit() {
  }

  modelChanged(model: Model): void {
    this.vehicleCreation.modelId = model.id;
  }

  get completed(): boolean {
    for(let key of Object.keys(this.vehicleCreation))
      if(!this.vehicleCreation[key])
        return false;
    return true;
  }

  createVehicle(): void {
    this.vehicleService.create(this.vehicleCreation)
                       .subscribe(
                         vehicle =>
                           this.router.navigateByUrl(`/vehicle/${vehicle.id}`)
                       );
  }
}
