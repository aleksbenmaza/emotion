import {Component, Input, OnInit} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Make} from "../../../../../model/input/Make";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-make',
  templateUrl: './make.component.html',
  styleUrls: ['./make.component.css']
})
export class MakeComponent implements OnInit {

  make: Make;

  @Input()
  name: string;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly vehicleService: VehicleService
  ) {
    this.vehicleService.getOneMake(this.route.snapshot.params['id'])
                       .subscribe(make => this.make = make);
  }

  ngOnInit() {
  }

  updateMake(): void {
    this.vehicleService.updateMake(this.route.snapshot.params['id'], this.name)
                       .subscribe();
  }

  deleteMake(): void {
    this.vehicleService.deleteOneMake(this.route.snapshot.params['id'])
                       .subscribe(
                         () =>
                           this.router.navigateByUrl('/marque/liste')
                       );
  }
}
