import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerRentalHomeComponent } from './customer-rental-home.component';

describe('CustomerRentalHomeComponent', () => {
  let component: CustomerRentalHomeComponent;
  let fixture: ComponentFixture<CustomerRentalHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerRentalHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerRentalHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
