import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {VehicleService} from "../../../../service/vehicle.service";
import {Model, VehicleType} from "../../../../../model/input/Model";
import {Memoize} from "typescript-memoize";

@Component({
  selector: 'app-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.css']
})
export class ModelsComponent implements OnInit {

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  @Memoize()
  get models(): Observable<Model[]> {
    return this.vehicleService.getModelsByVehicleType(
      VehicleType[
        VehicleType[
          this.route.snapshot.params['type']
          ]
        ]
    );
  }
}
