import {Component, Input, OnInit} from '@angular/core';
import {MakeCreation} from "../../../../../model/output/MakeCreation";
import {VehicleService} from "../../../../service/vehicle.service";
import {Router} from "@angular/router";
import {StringUtils} from "../../../../../utils/StringUtils";

@Component({
  selector: 'app-new-make',
  templateUrl: './new-make.component.html',
  styleUrls: ['./new-make.component.css']
})
export class NewMakeComponent implements OnInit {

  @Input()
  makeCreation: MakeCreation;

  constructor(
    private readonly vehicleService: VehicleService,
    private readonly router: Router
  ) {
    this.makeCreation = {
      code: null,
      name: null,
    };
  }

  ngOnInit() {
  }

  get completed(): boolean {
    return !StringUtils.areBlank([
      this.makeCreation.code,
      this.makeCreation.name,
    ]);
  }

  createMake(): void {
    this.vehicleService.createMake(this.makeCreation)
                       .subscribe(
                         make =>
                           this.router.navigateByUrl(`/marque/${make.id}`)
                       );
  }
}
