import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentalComponent } from './customer-rental.component';

describe('CustomerRentalComponent', () => {
  let component: RentalComponent;
  let fixture: ComponentFixture<RentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should createCustomer', () => {
    expect(component).toBeTruthy();
  });
});
