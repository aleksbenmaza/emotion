import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {VehicleService} from "../../../../service/vehicle.service";
import {VehicleSearch} from "../../../../../model/output/VehicleSearch";
import {Vehicle} from "../../../../../model/input/Vehicle";
import {VehicleType} from "../../../../../model/input/Model";
import {Model} from "../../../../../model/input/Model";
import {BusinessUtils} from "../../../../../utils/BusinessUtils";

@Component({
  selector: 'app-available-vehicles',
  templateUrl: './available-vehicles.component.html',
  styleUrls: ['./available-vehicles.component.css']
})
export class AvailableVehiclesComponent implements OnInit {

  vehicles: Vehicle[];

  @Input()
  search: VehicleSearch;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly vehicleService: VehicleService
  ) {}

  ngOnInit() {
    this.search = {
      modelVehicleType: null,
      modelId: null,
      modelMakeId: null,
      rentalPickupDate: null,
      rentalNumberOfDay: null,
    };

    for(const property of Object.keys(this.route.snapshot.queryParams))
      this.search[property] = this.route.snapshot.queryParams[property];

    this.vehicleService
        .get(this.search)
        .subscribe(vehicles => this.vehicles = vehicles);
  }

  isCar(model: Model): boolean {
    return BusinessUtils.isCar(model);
  }
}
