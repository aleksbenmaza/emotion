import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Type, User} from "../../../../model/input/User";

@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.css']
})
export class UserNavbarComponent implements OnInit {

  @Input()
  user: User;

  @Output("tokenDestroyed")
  eventEmitter: EventEmitter<undefined>;

  constructor() {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
  }

  destroyToken(): void {
    this.eventEmitter.emit();
  }

  isAdmin(user: User): boolean {
    return user.__type__ == Type.ADMIN;
  }
}
