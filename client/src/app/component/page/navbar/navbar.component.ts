import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Token} from "../../../../model/input/Token";
import {User} from "../../../../model/input/User";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input()
  unauthorized: boolean;

  @Input()
  user: User;

  @Output("tokenCreated")
  eventEmitter: EventEmitter<Token>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
  }

  tokenCreated(token: Token): void {
    this.user = token.user;
    this.eventEmitter.emit(token);
  }
}
