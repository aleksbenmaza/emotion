import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VehicleService} from "../../../../service/vehicle.service";
import {Model} from "../../../../../model/input/Model";

@Component({
  selector: 'app-model-autocomplete',
  templateUrl: './model-autocomplete.component.html',
  styleUrls: ['./model-autocomplete.component.css']
})
export class ModelAutocompleteComponent implements OnInit {

  models: Model[];

  @Input()
  model: Model;

  @Output("modelChanged")
  eventEmitter : EventEmitter<Model>;

  constructor(private readonly vehicleService: VehicleService) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
    this.reload();
  }

  emit(): void {
    this.eventEmitter.emit(this.model);
  }

  reload(makeId: number = null): void {
    if(makeId)
      this.vehicleService
          .getModels(makeId)
          .subscribe(models => this.models = models);
    else
      this.vehicleService
          .getAllModels()
          .subscribe(models => this.models = models);
  }

  formateName(model: Model): string {
    return model.name
      + (
        this.models.filter(m => m.name = model.name).length > 1 ?
          ' (' + model.make.name + ')' :
          ''
      );
  }
}
