import * as apiConfig from '../../config/api.json';

import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from "@angular/core";

@Injectable()
export abstract class Service {

  protected readonly apiConfig: any = apiConfig;

  constructor(protected readonly httpClient: HttpClient) {}

  protected toHttpParams(object: Object): HttpParams {
    let httpParams: HttpParams;
    let value: any;

    httpParams = new HttpParams;

    Object.keys(object).forEach(key => {
      if (value = object[key])
        httpParams = httpParams.append(key, value.toString());
    });

    return httpParams;
  }
}
