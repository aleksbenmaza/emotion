import {Injectable} from "@angular/core";
import {Type} from "../../model/input/User";
import {AuthGuard} from "./AuthGuard";

@Injectable()
export class AdminAuthGuard extends AuthGuard {

  protected support(): Type {
    return Type.ADMIN;
  }
}
