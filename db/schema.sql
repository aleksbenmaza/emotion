-----------------
--- SEQUENCES ---
-----------------

CREATE SEQUENCE seq_makes;
CREATE SEQUENCE seq_models;
CREATE SEQUENCE seq_vehicles;
CREATE SEQUENCE seq_users;
CREATE SEQUENCE seq_rentals;

---------------
--- DOMAINS ---
---------------

CREATE DOMAIN vehicle_type AS SMALLINT CHECK(VALUE BETWEEN 0 AND 1);

CREATE DOMAIN french_zip_code AS CHAR(5) CHECK(VALUE ~ E'^\\d{5}$');

CREATE DOMAIN plate_number AS CHAR(7) CHECK(VALUE ~ E'^[A-Z]{2}\\d{3}[A-Z]{2}$');

CREATE DOMAIN email_address AS VARCHAR CHECK(VALUE ~ E'^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');

--------------
--- TABLES ---
--------------

CREATE TABLE makes (
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('seq_makes'),
  code VARCHAR(4) UNIQUE NOT NULL,
  name VARCHAR
);

CREATE TABLE models (
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('seq_models'),
  make_id BIGINT REFERENCES makes ON DELETE CASCADE NOT NULL,
  code VARCHAR(4) NOT NULL,
  vehicle_type vehicle_type NOT NULL,
  name VARCHAR,
  UNIQUE(make_id, code)
);

CREATE TABLE vehicles (
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('seq_vehicles'),
  model_id BIGINT REFERENCES models ON DELETE CASCADE NOT NULL,
  serial_number CHAR(17) UNIQUE NOT NULL,
  plate_number CHAR(7) UNIQUE,
  color VARCHAR,
  km_number DECIMAL,
  purchase_date DATE,
  purchase_price NUMERIC(8, 2),
  rental_price NUMERIC(8, 2),
  loyalty_points INTEGER,
  hidden BOOLEAN DEFAULT FALSE
);

CREATE TABLE users (
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('seq_users'),
  email_address email_address UNIQUE NOT NULL,
  hash CHAR(128)
);

CREATE TABLE admins (
  id BIGINT PRIMARY KEY REFERENCES users ON DELETE CASCADE
);

CREATE TABLE customers (
  id BIGINT PRIMARY KEY REFERENCES users ON DELETE CASCADE,
  last_name VARCHAR,
  first_name VARCHAR,
  birth_date DATE,
  address VARCHAR,
  zip_code french_zip_code,
  city VARCHAR,
  mail_validated BOOLEAN DEFAULT FALSE
);

CREATE TABLE rentals (
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('seq_rentals'),
  vehicle_id BIGINT REFERENCES vehicles ON DELETE CASCADE NOT NULL,
  customer_id BIGINT REFERENCES customers ON DELETE CASCADE NOT NULL,
  pickup_date DATE NOT NULL,
  duration SMALLINT NOT NULL DEFAULT 1,
  overdue_duration SMALLINT,
  using_loyalty_points BOOLEAN DEFAULT FALSE,
  creation_time TIMESTAMP DEFAULT NOW(),
  UNIQUE(vehicle_id, pickup_date)
);

CREATE TABLE vehicles_loyalty_points_histories(
  id BIGINT REFERENCES vehicles ON DELETE CASCADE NOT NULL,
  creation_time TIMESTAMP DEFAULT NOW(),
  old_value INTEGER,
  PRIMARY KEY (id, creation_time)
);

-----------------
--- FUNCTIONS ---
-----------------

CREATE OR REPLACE FUNCTION rental_effective_loyalty_points(
  IN in_rental_id  BIGINT,
  IN in_vehicle_id BIGINT
) RETURNS INTEGER AS $$
SELECT COALESCE(old_value, loyalty_points)
FROM vehicles v
  LEFT JOIN rentals r
    ON v.id = vehicle_id
  LEFT JOIN vehicles_loyalty_points_histories vlph
    ON v.id = vlph.id AND r.creation_time < vlph.creation_time
WHERE v.id=in_vehicle_id AND r.id=in_rental_id
ORDER BY vlph.creation_time
LIMIT 1
$$ LANGUAGE sql;

-------------
--- VIEWS ---
-------------

CREATE VIEW void AS
  SELECT NULL;

CREATE VIEW mixed_users AS
  SELECT * FROM (
    SELECT
      'admin' AS type,
      u.id,
      u.email_address,
      u.hash,
      NULL AS last_name,
      NULL AS first_name,
      NULL AS birth_date,
      NULL AS address,
      NULL AS zip_code,
      NULL AS city
    FROM users u
      JOIN admins USING (id)
    UNION
    SELECT
      'customer',
      u.id,
      u.email_address,
      u.hash,
      c.last_name,
      c.first_name,
      c.birth_date,
      c.address,
      c.zip_code,
      c.city
    FROM users u
      JOIN customers c USING (id)
  ) mixed_users
ORDER BY CASE type WHEN 'admin' THEN 1 ELSE 0 END, id;

CREATE VIEW complete_customers AS
  SELECT * FROM users JOIN customers USING(id);

CREATE VIEW customer_loyalty_points AS
  SELECT
    id,
    COALESCE(
        (
          SELECT SUM(
                 rental_effective_loyalty_points(
                     rentals.id,
                     vehicles.id
                 ) *
                 duration
          ) / 10
          FROM rentals
            JOIN vehicles USING(id)
          WHERE customer_id=customers.id
    ) -
    (
      SELECT SUM(
          rental_effective_loyalty_points(
              rentals.id,
              vehicles.id
          ) *
          duration
      )
      FROM rentals
        JOIN vehicles USING(id)
      WHERE customer_id=customers.id AND
            using_loyalty_points
    ), 0) AS value
  FROM customers;

-----------------
--- FUNCTIONS ---
-----------------

CREATE OR REPLACE FUNCTION set_rentals_day_count() RETURNS TRIGGER AS $$
BEGIN
  IF NEW.duration < 1 THEN
    SET NEW.day_count = 1;
  END IF;
  RETURN NEW;
END $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION rental_overlaps(
  IN in_rental_id   BIGINT,
  IN in_pickup_date DATE,
  IN in_duration    SMALLINT,
  IN in_vehicle_id  BIGINT
) RETURNS BOOLEAN AS $$
  SELECT EXISTS(
      SELECT 1
      FROM rentals
      WHERE TSRANGE(pickup_date, pickup_date + duration, '[]') &&
            TSRANGE(in_pickup_date, in_pickup_date + in_duration, '[]')
        AND id <> in_rental_id AND vehicle_id=in_vehicle_id
  )
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION rental_overlaps(
  IN in_pickup_date DATE,
  IN in_duration    SMALLINT,
  IN in_vehicle_id  BIGINT
) RETURNS BOOLEAN AS $$
SELECT EXISTS(
    SELECT 1
    FROM rentals
    WHERE TSRANGE(pickup_date, pickup_date + duration, '[]') &&
          TSRANGE(in_pickup_date, in_pickup_date + in_duration, '[]')
          AND vehicle_id=in_vehicle_id
)
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION rental_duration_available(
  IN in_duration   SMALLINT,
  IN in_vehicle_id BIGINT
) RETURNS BOOLEAN AS $$
  SELECT EXISTS(
      SELECT 1
      FROM rentals r0
      WHERE NOT EXISTS(
          SELECT 1
          FROM rentals r1
          WHERE r0.vehicle_id=r1.vehicle_id AND
            r1.vehicle_id=in_vehicle_id AND
            r0.id <> r1.id AND
            r0.pickup_date < r1.pickup_date AND
            r1.pickup_date > NOW() AND
            r0.pickup_date + r0.duration - r1.pickup_date < in_duration
      )
  ) OR (
    SELECT CASE COUNT(*) WHEN 0 THEN TRUE ELSE FALSE END
    FROM rentals
  )
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION insert_into_vehicles_loyalty_points_histories(
) RETURNS TRIGGER AS $$
BEGIN
  IF OLD.loyalty_points IS NOT NULL AND OLD.loyalty_points <> NEW.loyalty_points THEN
    INSERT INTO vehicles_loyalty_points_histories
    VALUES (OLD.id, DEFAULT, OLD.loyalty_points);
  END IF;
  RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION enough_customer_loyalty_points(
  IN in_rental_id   BIGINT,
  IN in_duration    SMALLINT,
  IN in_customer_id BIGINT,
  IN in_vehicle_id  BIGINT
) RETURNS BOOLEAN AS $$
  SELECT value - loyalty_points * in_duration >= 0
  FROM rentals r
    JOIN customer_loyalty_points clp ON customer_id = r.id
    JOIN vehicles v ON vehicle_id = v.id
  WHERE r.id <> in_rental_id AND clp.id=in_customer_id AND v.id=in_vehicle_id
$$ LANGUAGE sql;

----------------
--- TRIGGERS ---
----------------

CREATE TRIGGER before_rentals
BEFORE INSERT OR UPDATE ON rentals
FOR EACH ROW
EXECUTE PROCEDURE set_rentals_day_count();

CREATE TRIGGER after_update_vehicles
AFTER UPDATE ON vehicles
FOR EACH ROW
EXECUTE PROCEDURE insert_into_vehicles_loyalty_points_histories();

-------------------
--- CONSTRAINTS ---
-------------------

ALTER TABLE rentals
ADD CHECK(
  creation_time IS NOT NULL OR
  NOT rental_overlaps(id, pickup_date, duration, vehicle_id)
);

ALTER TABLE rentals
ADD CHECK(
  creation_time IS NOT NULL OR
  NOT using_loyalty_points OR
  enough_customer_loyalty_points(id, duration, customer_id, vehicle_id)
);

--------------
--- INSERT ---
--------------

INSERT INTO makes
VALUES
  (DEFAULT, 'REN', 'Renault'),
  (DEFAULT, 'BMW', 'BMW'),
  (DEFAULT, 'CHV', 'Chevrolet'),
  (DEFAULT, 'NIS', 'Nissan'),
  (DEFAULT, 'TES', 'Tesla'),
  (DEFAULT, 'CTR', 'Citroën'),
  (DEFAULT,	'NIU', 'NIU'),
  (DEFAULT,	'2TW', '2Twenty');


INSERT INTO models
VALUES
  (DEFAULT, 2, 'I3', 0, 'i3'),
  (DEFAULT, 3, 'BOE', 0, 'Bolt EV'),
  (DEFAULT, 1, 'ZOE', 0, 'Zoé'),
  (DEFAULT, 6, 'C1', 0, 'C1'),
  (DEFAULT, 5, 'MDS', 0, 'Model S'),
  (DEFAULT, 6, 'LEA', 0, 'Leaf'),
  (DEFAULT,	7, 'N1S',	1, 'N1S'),
  (DEFAULT,	8, 'RET',	1, 'Retros'),
  (DEFAULT, 7, 'MSP',	1, 'M-Series PRO');

INSERT INTO vehicles
VALUES
  (DEFAULT, 1, '1FTFE24NXGHB89071', 'IA391MB', 'bleue', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT, 2, '1FTYR10U2WUA57450', 'NR900PG', 'blanc', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT, 3, 'JM1BL1V75C1647570', 'LZ283GM', 'bleu', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT, 4, '1XKADU9X97R130787', 'LX830MA', 'blanc', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT, 5, '1FTCR11U9PUA39327', 'CK034JZ', 'blanc', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT, 6, 'SALJY1246TA718694', 'MS932ZM', 'noire', 555, '2017-07-12', 24056.55, 99.00, 2500, FALSE),
  (DEFAULT,	7,	'JTDKN3DU6F1892850', 'FS856DE',	'blanc', 555, '2018-08-22',	2899.00, 29.00,	1000, FALSE),
  (DEFAULT,	7,	'1NXBR12E8WZ801298', 'DR568FG',	'rouge', 555,	'2018-08-22',	2899.00, 29.00, 1000, FALSE),
  (DEFAULT,	7,	'1B7MC33751J696594', 'MP585DF',	'noire', 555,	'2018-08-22',	2899.00, 29.00, 1000, FALSE),
  (DEFAULT,	8,	'1HTSCABN82H475935', 'JI423SE',	'bleu', 555, '2018-08-22', 2890.00,	29.00, 1500, FALSE),
  (DEFAULT,	9,	'1FV6HLBA3SL578450', 'SE526FG',	'blanc', 555,	'2018-08-22', 2398.00, 29.00, 869, FALSE),
  (DEFAULT,	9,	'4V1WDBRG6RN666842', 'RT856VG',	'gris', 555, '2018-08-22', 2398.00,	29.00, 869, FALSE);


INSERT INTO users
VALUES
  (DEFAULT, 'valid@tion.test', NULL),
  (DEFAULT, 'test@test.test', '652c08be7e63a60cd1eec9a2c3e2b9847b41ce7dd50a41bce8e5e026ece24ded175cb7557a2f6ce6d26705696b19ce208b49d2ce6f9c6832363da6f558c013a8'),
  (DEFAULT, 'admin@admin.admin', 'e48aa91375ce293baa923c761371a3a5cbe8f2f7afed31ac9cc62fc84ead33bf88f683a9ba644047495540264126cf02be9256af3242e4f0eceed79bea861fa0');

INSERT INTO admins
VALUES(3);

INSERT INTO customers
VALUES
  (1, 'test', 'test', '1993-01-01', NULL, NULL, NULL, DEFAULT),
  (2, 'Doe', 'John', '1987-03-05', '1 rue de la paix', '75007', 'Paris', TRUE);

INSERT INTO rentals
VALUES
  (DEFAULT , 1, 1, '2018-07-13', 3, NULL, DEFAULT),
  (DEFAULT , 3, 2, '2018-07-20', 2, NULL, DEFAULT);