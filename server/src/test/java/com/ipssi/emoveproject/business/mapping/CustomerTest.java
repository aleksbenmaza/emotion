package com.ipssi.emoveproject.business.mapping;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class CustomerTest {

    private final LocalDate birthDate = LocalDate.of(1952, Month.FEBRUARY, 1);

    private final String firstName = "John";
    private final String lastName = "Doe";
    private final String address = "45 rue le pont";
    private final String city = "Paris";
    private final String zipCode = "75012";
    private final String emailAddress = "name@name.fr";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void should_create_customer() {
        //Given When
        Customer customer = new Customer(lastName, firstName, birthDate, address, city, zipCode, emailAddress);

        //Then
        assertThat(customer.getFirstName()).isEqualTo(firstName);
        assertThat(customer.getLastName()).isEqualTo(lastName);
        assertThat(customer.getBirthDate()).isEqualTo(birthDate);
        assertThat(customer.getAddress()).isEqualTo(address);
        assertThat(customer.getCity()).isEqualTo(city);
        assertThat(customer.getZipCode()).isEqualTo(zipCode);
        assertThat(customer.getEmailAddress()).isEqualTo(emailAddress);
    }

    @Test
    public void should_throw_exception_when_last_name_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Last name cannot be blank");

        // When
        new Customer("", firstName, birthDate, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_last_name_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Last name cannot be blank");

        // When
        new Customer(null, firstName, birthDate, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_first_name_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("First name cannot be blank");

        // When
        new Customer(lastName, "", birthDate, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_first_name_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("First name cannot be blank");

        // When
        new Customer(lastName, null, birthDate, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_birth_date_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Birth date cannot be null");

        // When
        new Customer(lastName, firstName, null, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_customer_is_underage() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("You need to have majority for rental vehicle");

        // Given
        final LocalDate underage = LocalDate.of(2014, Month.FEBRUARY, 1);

        // When
        new Customer(lastName, firstName, underage, address, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_address_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Address cannot be blank");

        // When
        new Customer(lastName, firstName, birthDate, "", city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_address_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Address cannot be blank");

        // When
        new Customer(lastName, firstName, birthDate, null, city, zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_city_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("City cannot be bank");

        // When
        new Customer(lastName, firstName, birthDate, address, "", zipCode, emailAddress);
    }

    @Test
    public void should_throw_exception_when_city_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("City cannot be bank");

        // When
        new Customer(lastName, firstName, birthDate, address, null, zipCode, emailAddress);
    }


    @Test
    public void should_throw_exception_when_zip_code_is_blank() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Zip code cannot be blank");

        // When
        new Customer(lastName, firstName, birthDate, address, city, "", emailAddress);
    }


    @Test
    public void should_throw_exception_when_zip_code_is_null() {
        // Expect
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Zip code cannot be blank");

        // When
        new Customer(lastName, firstName, birthDate, address, city, null, emailAddress);
    }

    @Test
    public void should_throw_exception_when_zip_code_is_superior_a_5_character() {
        // Expect
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Zip code cannot be superior to five character");

        // Given
        String toLongZipCode = "750012";

        // When
        new Customer(lastName, firstName, birthDate, address, city, toLongZipCode, emailAddress);
    }
}