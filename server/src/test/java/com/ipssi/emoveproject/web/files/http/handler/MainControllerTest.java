package com.ipssi.emoveproject.web.files.http.handler;

import com.ipssi.emoveproject.web.common.FilesServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;

import java.nio.file.Paths;

import static java.net.URLDecoder.decode;
import static java.nio.file.Files.readAllBytes;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestPropertySource("classpath:override.properties")
public class MainControllerTest extends ControllerTest implements FilesServletContextConsumer {

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_201_when_onPost() throws Exception {

        final byte[] content;
        final MockMultipartFile file;
        final String responseBody;

        content = readAllBytes(
                Paths.get(
                        decode(
                                getClass().getClassLoader()
                                        .getResource("test.jpg")
                                        .getFile(),
                                "UTF-8"
                        )
                )
        );

        file = new MockMultipartFile(
                "file",
                "test.jpeg",
                "application/jpeg",
                content
        );

        responseBody = getMockMvc().perform(
                multipart("/makes/1").file(file)
        ).andExpect(
                status().isCreated()
        ).andReturn().getResponse().getContentAsString();

        assertEquals("makes/1.jpeg", responseBody);
        assertArrayEquals(
                content,
                getMockMvc().perform(
                        get("/public/makes/1.jpeg")
                ).andReturn()
                 .getResponse()
                 .getContentAsByteArray()
        );
    }
}