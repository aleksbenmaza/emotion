package com.ipssi.emoveproject.web.common.business.logic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.slf4j.LoggerFactory.getLogger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath*:/applicationContext.xml")
public class UserServiceImplTest {

    private static final Logger logger = getLogger(UserServiceImplTest.class);

    @Autowired
    private UserServiceImpl userService;

    @Test
    public void getOne() throws Exception {

    }

    @Test
    public void getOne1() throws Exception {

    }

    @Test
    public void delete() throws Exception {

    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void getCustomers() throws Exception {

    }

    @Test
    public void getAllCustomers() throws Exception {

    }

    @Test
    public void getOneCustomer() throws Exception {

    }

    @Test
    public void createCustomer() throws Exception {

    }

    @Test
    public void updateCustomer() throws Exception {

    }

    @Test
    public void hash() throws Exception {
        logger.info(userService.hash("toto@toto.toto", "pimpampoum"));
    }

}