package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.ModelCreation;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import static com.ipssi.emoveproject.business.mapping.VehicleType.SCOOTER;
import static java.math.BigInteger.ONE;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ModelControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    public void should_return_status_200_when_on_get() throws Exception {

        getMockMvc().perform(get("/models"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/models/1"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/models").param("ids", "1", "2"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/models").param("name", "a"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/models").param("makeId", "1")
                                                     .param("name", "a"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_201_when_on_post() throws Exception {

        final ModelCreation modelCreation;

        modelCreation = new ModelCreation(ONE, "TOTO", SCOOTER, "Totoyoyotata");

        getMockMvc().perform(
                post("/models").contentType(APPLICATION_JSON)
                        .content(
                                gson.toJson(modelCreation)
                        )
        ).andExpect(
                status().isCreated()
        );
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_on_delete() throws Exception {

        getMockMvc().perform(delete("/models/1"))
                .andExpect(status().isNoContent());

        getMockMvc().perform(delete("/models?ids=2&ids=3"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void should_return_status_204_when_onPatch() throws Exception {

        final String newName;

        newName = "patched name";

        getMockMvc().perform(
                patch("/models/1")
                        .content(
                                newName
                        )
        ).andExpect(
                status().isNoContent()
        );
    }
}