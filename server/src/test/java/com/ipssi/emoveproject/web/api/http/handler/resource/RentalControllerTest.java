package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.ipssi.emoveproject.web.api.model.input.OverdueDurationUpdate;
import com.ipssi.emoveproject.web.common.ApiServletContextConsumer;
import com.ipssi.emoveproject.web.common.http.handler.ControllerTest;
import org.junit.Test;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.HashMap;
import java.util.Map;

import static com.albema.common.util.ObjectUtils.toMap;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser(authorities = "ADMIN")
public class RentalControllerTest extends ControllerTest implements ApiServletContextConsumer {

    @Test
    public void should_return_200_when_onGet() throws Exception {
        getMockMvc().perform(get("/rentals"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/rentals/1"))
                .andExpect(status().isOk());

        getMockMvc().perform(get("/rentals").param("ids", "1", "2"))
                .andExpect(status().isOk());
    }

    @Test
    public void should_return_status_204_when_onPatch() throws Exception {

        final Map<String, String> rentalUpdateMap;

        rentalUpdateMap = new HashMap<>();

        toMap(
                new OverdueDurationUpdate((byte) 3)
        ).forEach(
                (k, v) -> rentalUpdateMap.put(k, v.toString())
        );

        rentalUpdateMap.put("__type__", OverdueDurationUpdate.class.getSimpleName());

        getMockMvc().perform(
                patch("/rentals/1").content(gson.toJson(rentalUpdateMap)).contentType(APPLICATION_JSON)
        ).andExpect(
                status().isNoContent()
        );
    }

    @Test
    public void should_return_status_204_when_onDelete() throws Exception {

        getMockMvc().perform(delete("/rentals/1"))
                .andExpect(status().isNoContent());

        getMockMvc().perform(delete("/rentals").param("ids", "1", "2"))
                .andExpect(status().isNoContent());
    }
}