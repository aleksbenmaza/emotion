package com.ipssi.emoveproject.web.common.helper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.util.Locale.FRANCE;
import static java.util.Locale.setDefault;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:applicationContext.xml")
public class RentalScheduledTasksTest {

    @Value("${mailbox}")
    private String mailbox;

    @Autowired
    private RentalScheduledTasks rentalScheduledTasks;

    @Test
    public void should_send_mails_every_day_on_10am_when_proceedOverdueDurations() throws Exception {
        final RestTemplate restTemplate;

        restTemplate = new RestTemplate();

        restTemplate.delete("http://" + mailbox + "/email/all");

        setDefault(FRANCE);

        rentalScheduledTasks.proceedOverdueDurations();

        assertNotEquals(
                0,
                restTemplate.getForObject("http://" + mailbox + "/email", List.class)
                            .size()
        );
    }
}