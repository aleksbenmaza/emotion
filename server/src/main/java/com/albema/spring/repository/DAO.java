package com.albema.spring.repository;

import com.albema.common.orm.entity.BaseEntity;
import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.annotation.PreDestroy;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Arrays.*;
import static java.util.Arrays.asList;

import static org.apache.commons.lang3.StringUtils.join;

public abstract class DAO {

    private final SessionFactory sessionFactory;

    protected DAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * TODO finish
     */
    @SuppressWarnings("WeakerAccess")
    protected static Class resolveCommonAncestorEntityClass(BaseEntity... entities) {
        return null;
    }

    @PreDestroy
    protected void destroyed() {
        getCurrentSession().close();
        sessionFactory.close();
    }

    protected final Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    protected <E extends BaseEntity> List<E> find0(Class<E> entityClass) {
        final Query<E> query;

        return getCurrentSession().createQuery(" FROM " + entityClass.getName(), entityClass)
                .list();
    }

    protected <E extends IdentifiableById> List<E> find0(Class<E> entityClass, BigInteger... ids) {
        final String stm;

        stm = "FROM " + entityClass.getName() + " c WHERE c.id IN :ids";

        return getCurrentSession().createQuery(stm, entityClass)
                .setParameterList("ids", asList(ids))
                .list();
    }

    protected <E extends IdentifiableById> E find0(Class<E> entityClass, BigInteger id) {
        final String stm;

        stm = "FROM " + entityClass.getName() + " c WHERE c.id = :id";

        return getCurrentSession().createQuery(stm, entityClass)
                                  .setParameter("id", id)
                                  .uniqueResult();
    }

    protected <E extends BaseEntity, T> List<E> find0(
            Class<E> entityClass,
            T[] tuples,
            String[] attributes,
            Function<T, Serializable>[] getters
    ) {
        final String stm;
        final Query<E> query;

        if (getters.length + attributes.length == 0 || getters.length != attributes.length)
            throw new IllegalArgumentException();

        stm = "FROM " + entityClass.getName() + " "
                + "WHERE " + createCompositeInClause(tuples.length, attributes);

        query = getCurrentSession().createQuery(stm, entityClass);

        bindCompositeInClause(query, tuples, attributes, getters);

        return query.list();
    }

    protected void save0(RecordableEntity... entities) {
        final Session session;

        session = getCurrentSession();

        stream(entities).forEach(session::saveOrUpdate);

        session.flush();
    }

    protected void save0(RecordableEntity entity) {
        save0(new RecordableEntity[] {entity});
    }

    protected int remove0(Class<? extends IdentifiableById> entityClass, BigInteger... ids) {
        final String stm;

        if (ids.length == 0)
            return 0;

        stm = "DELETE FROM " + entityClass.getName() + " c WHERE c.id IN :ids";

        return getCurrentSession().createQuery(stm)
                .setParameter("ids", asList(ids))
                .executeUpdate();
    }

    protected <E extends BaseEntity, T> int remove0(
            Class<E> entityClass,
            T[] tuples,
            String[] attributes,
            Function<T, Serializable>[] getters
    ) {
        final String stm;
        final Query<E> query;

        if (getters.length + attributes.length == 0 || getters.length != attributes.length)
            throw new IllegalArgumentException();

        stm = "DELETE FROM " + entityClass.getName() + " "
                + "WHERE " + createCompositeInClause(tuples.length, attributes);

        query = getCurrentSession().createQuery(stm, entityClass);

        bindCompositeInClause(query, tuples, attributes, getters);

        return query.executeUpdate();
    }

    protected boolean remove0(RecordableEntity entity) {
        if (!getCurrentSession().contains(entity))
            return false;
        getCurrentSession().delete(entity);
        return true;
    }

    @SuppressWarnings("unchecked")
    protected int remove0(RecordableEntity... entities) {
        final String stm;

        Class<? extends RecordableEntity> entityClass;

        if (entities.length == 0)
            return 0;

        entityClass = resolveCommonAncestorEntityClass(entities);

        if (entityClass == null)
            throw new IllegalArgumentException();

        stm = "DELETE FROM " + entityClass.getName() + " e "
                + "WHERE e IN :entities";

        return getCurrentSession().createQuery(stm)
                .setParameter("entities", asList(entities))
                .executeUpdate();
    }

    private <E extends BaseEntity, T> void bindCompositeInClause(
            Query<E> query,
            T[] tuples,
            String[] attributes,
            Function<T, Serializable>[] getters
    ) {
        int i;
        int j;

        for (i = 0; i < tuples.length; ++i)
            for (j = 0; j < attributes.length; ++j)
                query.setParameter(attributes[j] + i, getters[j].apply(tuples[i]));
    }

    private <T> String createCompositeInClause(int size, String[] attributes) {
        final String orStmFormat;

        final ArrayList<String> orStms;

        int i;

        if (attributes.length == 0)
            throw new IllegalArgumentException();

        orStmFormat = join(attributes, "%1$i AND ") + "%1$i";

        orStms = new ArrayList<>();

        for (i = 0; i < size; ++i)
            orStms.add(format(orStmFormat, i));

        return '(' + join(orStms, " OR ") + ')';
    }
}