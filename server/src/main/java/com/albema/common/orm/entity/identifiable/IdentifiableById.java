package com.albema.common.orm.entity.identifiable;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public interface IdentifiableById {

    long NULL_ID = 0;

    Comparator<IdentifiableById> comparator = (o1, o2) -> {
        if (o1 == o2)
            return 0;
        if (o1 == null)
            return -1;
        if (o2 == null)
            return +1;
        if (o1.getId().equals(o2.getId()))
            return 0;
        return o1.getId().compareTo(o1.getId());
    };

    static <T extends IdentifiableById> List<T> toSortedList(Set<T> identifiableByIds) {
        ArrayList<T> resultList;
        resultList = new ArrayList<>(identifiableByIds);
        resultList.sort(comparator);
        return resultList;
    }

    BigInteger getId();

    @Override
    int hashCode();

    @Override
    boolean equals(Object that);
}