package com.albema.common.orm.entity;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Immutable
@Table(name = "void")
public class Void implements Serializable {

    @Id
    private Boolean value;
}
