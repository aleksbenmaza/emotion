package com.albema.common.orm.repository;

import com.albema.common.orm.entity.identifiable.IdentifiableById;

import java.math.BigInteger;
import java.util.List;

public interface Finder<T extends IdentifiableById> {

    List<T> find();

    List<T> find(BigInteger... ids);

    T find(BigInteger id);
}