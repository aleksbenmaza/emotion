package com.albema.common.orm.repository;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;

import java.math.BigInteger;

public interface Remover<T extends RecordableEntity & IdentifiableById> {

    boolean remove(T entity);

    @SuppressWarnings("unchecked")
    int remove(T... entities);

    boolean remove(BigInteger id);

    int remove(BigInteger... ids);
}
