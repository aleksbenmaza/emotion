package com.albema.common.http.error;

import com.albema.common.http.view.ForwardingView;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.RedirectView;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;

@UtilityClass
@SuppressWarnings("unused")
public final class CustomHttpExceptions {

    public abstract class HttpException extends RuntimeException {

        public abstract HttpStatus getHttpStatus();
    }

    @ResponseStatus(METHOD_NOT_ALLOWED)
    public class MethodNotAllowedException extends RuntimeException {
    }

    @ResponseStatus(BAD_REQUEST)
    public class BadRequestException extends RuntimeException {
    }

    @ResponseStatus(FORBIDDEN)
    public class ResourceForbiddenException extends WithAbstractUrlBasedViewException {

        @Override
        public HttpStatus getHttpStatus() {
            return FORBIDDEN;
        }
    }

    @ResponseStatus(NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
    }

    @ResponseStatus(UNAUTHORIZED)
    public class UnauthorizedRequestException extends WithAbstractUrlBasedViewException {

        @Override
        public HttpStatus getHttpStatus() {
            return UNAUTHORIZED;
        }
    }

    @ResponseStatus(BAD_REQUEST)
    public class CommandNotValidatedException extends HttpException {

        private Object errors;

        private ModelAndView modelAndView;

        public CommandNotValidatedException() {
        }

        public CommandNotValidatedException(Object errors) {
            this.errors = errors;
        }

        public CommandNotValidatedException withModelAndView(ModelAndView modelAndView) {
            modelAndView.setStatus(getHttpStatus());
            this.modelAndView = modelAndView;
            return this;
        }

        public Object getErrors() {
            return errors;
        }

        @Override
        public HttpStatus getHttpStatus() {
            return BAD_REQUEST;
        }

        public ModelAndView getModelAndView() {
            return modelAndView;
        }
    }


    public abstract class WithAbstractUrlBasedViewException extends HttpException {

        private AbstractUrlBasedView view;

        public WithAbstractUrlBasedViewException withForwarding(String uri) {
            view = new ForwardingView(uri);
            ((ForwardingView) view).setHttpStatus(getHttpStatus());
            return this;
        }

        public WithAbstractUrlBasedViewException withRedirect(String uri) {
            view = new RedirectView(uri);
            ((RedirectView) view).setStatusCode(getHttpStatus());
            return this;
        }

        public AbstractUrlBasedView getView() {
            return view;
        }
    }
}