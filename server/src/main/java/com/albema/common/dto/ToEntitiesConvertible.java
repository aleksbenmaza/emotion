package com.albema.common.dto;

import com.albema.common.orm.entity.identifiable.IdentifiableById;

public interface ToEntitiesConvertible<T extends IdentifiableById> {

    T[] toEntities();
}
