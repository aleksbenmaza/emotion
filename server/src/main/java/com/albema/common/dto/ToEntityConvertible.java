package com.albema.common.dto;

import com.albema.common.orm.entity.identifiable.IdentifiableById;

public interface ToEntityConvertible<T extends IdentifiableById> {

    T toEntity();
}
