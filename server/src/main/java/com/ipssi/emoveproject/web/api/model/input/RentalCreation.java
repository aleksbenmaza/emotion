package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.ExistingEntity;
import com.albema.spring.validation.Mapping;
import com.albema.spring.validation.UniqueEntityProperties;
import com.fatboyindustrial.gsonjavatime.LocalDateConverter;
import com.google.gson.annotations.JsonAdapter;
import com.ipssi.emoveproject.business.logic.RentalService;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.Rental;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import com.ipssi.emoveproject.web.api.model.input.validation.RentalOverlapForbidden;
import lombok.*;

import javax.validation.constraints.Future;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;

@Getter
@ToString
@EqualsAndHashCode
@UniqueEntityProperties(
        entityClass = Rental.class,
        mappings = {
                @Mapping(attribute = "pickupDate"),
                @Mapping(
                        attribute = "vehicleId",
                        entityProperty = "vehicle.id"
                )
        }
)
@RentalOverlapForbidden
@NoArgsConstructor
@AllArgsConstructor
public class RentalCreation implements RentalService.Creation, Serializable {

    @ExistingEntity(Vehicle.class)
    private BigInteger vehicleId;

    @ExistingEntity(Customer.class)
    private BigInteger customerId;

    @Future
    @JsonAdapter(LocalDateConverter.class)
    private LocalDate pickupDate;

    @Positive
    private byte duration;

    private boolean usingLoyaltyPoints;
}
