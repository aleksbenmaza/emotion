package com.ipssi.emoveproject.web.files.helper;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class FileManager {

    private final String fileUploadPath;

    public String save(
            final String        directory,
            final String        name,
            final MultipartFile multipartFile
    ) throws IOException {
        final String shortName;
        final String fullName;
        final File file;

        new File(fileUploadPath + directory).mkdirs();

        shortName = directory +
                    '/' +
                    name +
                    '.' +
                    multipartFile.getContentType().split("/")[1];

        fullName = fileUploadPath + shortName;

        file = new File(fullName);

        multipartFile.transferTo(file);

        return shortName;
    }

    FileManager(@Value("${files.uploadPath}") String fileUploadPath) {
        this.fileUploadPath = fileUploadPath;
    }
}
