package com.ipssi.emoveproject.web.common.business.repository;

import com.albema.spring.repository.DAO;
import com.ipssi.emoveproject.business.mapping.Rental;
import com.ipssi.emoveproject.business.repository.RentalDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

@Repository
class RentalDAOImpl extends DAO implements RentalDAO {

    @Override
    public void save(Rental entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void save(Rental... entities) {
        save0(entities);
    }

    @Override
    public List<Rental> find() {
        return find0(Rental.class);
    }

    @Override
    public List<Rental> find(BigInteger... ids) {
        return find0(Rental.class, ids);
    }

    @Override
    public boolean remove(Rental entity) {
        return remove0(entity);
    }

    @Override
    public Rental find(BigInteger id) {
        return find0(Rental.class, id);
    }

    @Override
    public int remove(Rental... entities) {
        return remove0(entities);
    }

    @Override
    public boolean remove(BigInteger id) {
        return remove0(Rental.class, id) == 0;
    }

    @Override
    public int remove(BigInteger... ids) {
        return remove0(Rental.class, ids);
    }

    @Override
    public List<Rental> findByCustomerId(BigInteger id) {
        final String stm;

        stm = "FROM " + Rental.class.getName() + " "
                + "WHERE customer.id=:id";

        return getCurrentSession().createQuery(stm, Rental.class)
                .setParameter("id", id)
                .list();
    }

    @Override
    public Rental findByCustomerId(BigInteger rentalId, BigInteger customerId) {
        final String stm;

        stm = "FROM " + Rental.class.getName() + " "
                + "WHERE id=:rentalId AND customer.id=:customerId";

        return getCurrentSession().createQuery(stm, Rental.class)
                                  .setParameter("rentalId", rentalId)
                                  .setParameter("customerId", customerId)
                                  .uniqueResult();
    }

    @Override
    public List<Rental> find(
            LocalDate maxExclusiveDropOffDate,
            Integer   overdueDuration
    ) {
        final String        stm;
        final Query<Rental> query;

        stm = "FROM " + Rental.class.getName() + " "
            + "WHERE pickupDate + numberOfDays < :maxExclusiveDropOffDate AND "
            +   (
                    overdueDuration != null ?
                            "numberOfDaysLate=:overdueDuration" :
                            "numberOfDaysLate IS NULL"
            );

        query = getCurrentSession().createQuery(stm, Rental.class)
                                   .setParameter(
                                           "maxExclusiveDropOffDate",
                                           maxExclusiveDropOffDate
                                   );

        if(overdueDuration != null)
            query.setParameter(
                    "numberOfDaysLate",
                    overdueDuration
            );

        return query.list();
    }

    RentalDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
