package com.ipssi.emoveproject.web.api.http.handler.resource;

import com.albema.common.http.error.CustomHttpExceptions;
import com.ipssi.emoveproject.business.logic.RentalService;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.web.api.model.input.CustomerCreation;
import com.ipssi.emoveproject.web.api.model.input.CustomerUpdate;
import com.ipssi.emoveproject.web.api.model.input.RentalCreation;
import com.ipssi.emoveproject.web.api.model.output.CustomerDTO;
import com.ipssi.emoveproject.web.api.model.output.RentalDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.albema.common.util.ObjectUtils.ifNotNull;
import static com.ipssi.emoveproject.web.api.model.output.DTO.fromCollection;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RequiredArgsConstructor
@RestController
@RequestMapping("/customers")
class CustomerController {

    private final UserService userService;

    private final RentalService rentalService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public List<CustomerDTO> onGet() {
        return fromCollection(userService.getAllCustomers(), CustomerDTO::new);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping(params = "ids")
    public List<CustomerDTO> onGet(@RequestParam BigInteger[] ids) {
        return fromCollection(userService.getCustomers(ids), CustomerDTO::new);
    }

    @PreAuthorize(
            "hasAuthority('ADMIN') OR " +
                    "hasAuthority('CUSTOMER') AND #id.toString().equals(principal.username)"
    )
    @GetMapping("/{id}")
    public CustomerDTO onGet(@PathVariable BigInteger id) {
        return ifNotNull(userService.getOneCustomer(id), CustomerDTO::new);
    }

    @PreAuthorize("isAnonymous()")
    @PostMapping
    @ResponseStatus(CREATED)
    public CustomerDTO onPost(
            @Valid @RequestBody CustomerCreation customerCreation
    ) throws NoSuchAlgorithmException {
        return new CustomerDTO(userService.createCustomer(customerCreation));
    }

    @PreAuthorize(
            "hasAuthority('ADMIN') OR " +
                    "hasAuthority('CUSTOMER') AND #id.toString().equals(principal.username)"
    )
    @ResponseStatus(NO_CONTENT)
    @PatchMapping("/{id}")
    public void onPatch(
            @PathVariable BigInteger     id,
            @RequestBody  CustomerUpdate update
    ) {
        userService.updateCustomer(id, update);
    }

    @PreAuthorize(
            "hasAuthority('ADMIN') OR " +
                    "hasAuthority('CUSTOMER') AND " +
                    "#customerId.toString().equals(principal.username)"
    )
    @GetMapping("/{customerId}/rentals")
    public List<RentalDTO> onRentalGet(@PathVariable BigInteger customerId) {
        return fromCollection(
                rentalService.getByCustomerId(customerId),
                RentalDTO::new
        );
    }

    @PreAuthorize(
            "hasAuthority('ADMIN') OR " +
                    "hasAuthority('CUSTOMER') AND " +
                    "#customerId.toString().equals(principal.username)"
    )
    @GetMapping("/{customerId}/rentals/{rentalId}")
    public RentalDTO onRentalGet(
            @PathVariable BigInteger customerId,
            @PathVariable BigInteger rentalId
    ) {
        return ifNotNull(
                rentalService.getOneByCustomerId(rentalId, customerId),
                RentalDTO::new
        );
    }
    @PreAuthorize(
            "hasAuthority('ADMIN') OR " +
                    "hasAuthority('CUSTOMER') AND " +
                    "#customerId.toString().equals(principal.username)"
    )
    @ResponseStatus(CREATED)
    @PostMapping("/{customerId}/rentals")
    public RentalDTO onRentalPost(
                   @PathVariable BigInteger     customerId,
            @Valid @RequestBody  RentalCreation rentalCreation
    ) {
        if(!customerId.equals(rentalCreation.getCustomerId()))
            throw new CustomHttpExceptions.BadRequestException();

        return new RentalDTO(rentalService.create(rentalCreation));
    }
}
