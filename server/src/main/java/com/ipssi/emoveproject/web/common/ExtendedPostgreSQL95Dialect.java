package com.ipssi.emoveproject.web.common;

import org.hibernate.dialect.PostgreSQL95Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;

import static org.hibernate.type.BooleanType.INSTANCE;

public class ExtendedPostgreSQL95Dialect extends PostgreSQL95Dialect {

    public ExtendedPostgreSQL95Dialect() {
        super();
        registerFunction(
                "doesRentalOverlap",
                new StandardSQLFunction(
                        "rental_overlaps",
                        INSTANCE
                )
        );
        registerFunction(
                "isRentalDurationAvailable",
                new StandardSQLFunction(
                        "rental_duration_available",
                        INSTANCE
                )
        );
        registerFunction(
                "areEnoughLoyaltyPoints",
                new StandardSQLFunction(
                        "enough_loyalty_points",
                        INSTANCE
                )
        );
    }
}
