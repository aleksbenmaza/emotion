package com.ipssi.emoveproject.web.common.business.repository;

import com.albema.spring.repository.DAO;
import com.ipssi.emoveproject.business.mapping.Make;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.business.repository.MakeDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
class MakeDAOImpl extends DAO implements MakeDAO {

    @Override
    public List<Make> find() {
        return find0(Make.class);
    }

    @Override
    public List<Make> find(BigInteger... ids) {
        return find0(Make.class, ids);
    }

    @Override
    public Make find(BigInteger id) {
        return find0(Make.class, id);
    }

    @Override
    public List<Make> findByNameLike(String nameLike) {
        final String stm;

        stm = "FROM " + Make.class.getName() + " " +
              "WHERE LOWER(name) LIKE LOWER(:nameLike)";

        return getCurrentSession().createQuery(stm, Make.class)
                                  .setParameter("nameLike", nameLike + "%")
                                  .list();
    }

    @Override
    public List<Make> findByVehicleType(VehicleType vehicleType) {
        final String stm;

        stm = "SELECT DISTINCT make " +
              "FROM " + Make.class.getName() + " make " +
              "JOIN make.models AS model WITH model.vehicleType=:vehicleType " +
              "ORDER BY make.name";

        return getCurrentSession().createQuery(stm, Make.class)
                                  .setParameter("vehicleType", vehicleType)
                                  .list();
    }

    @Override
    public int remove(BigInteger... ids) {
        return remove0(Make.class, ids);
    }

    @Override
    public boolean remove(BigInteger id) {
        return remove0(Make.class, id) != 0;
    }

    @Override
    public boolean remove(Make make) {
        return remove0(make);
    }

    @Override
    public int remove(Make... makes) {
        return remove0(makes);
    }

    @Override
    public void save(Make entity) {
        save0(entity);
    }

    @Override
    public void save(Make... entities) {
        save0(entities);
    }

    MakeDAOImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
