package com.ipssi.emoveproject.web.api.model.input;

import com.ipssi.emoveproject.business.logic.VehicleService;
import lombok.*;

@Getter
@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class RentalPriceAndLoyaltyPointsUpdate extends VehicleUpdate implements VehicleService.RentalPriceAndLoyaltyPointsUpdate {

    private Float rentalPrice;

    private Integer loyaltyPoints;
}
