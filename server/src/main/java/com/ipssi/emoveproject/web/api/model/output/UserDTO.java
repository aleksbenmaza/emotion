package com.ipssi.emoveproject.web.api.model.output;

import com.google.gson.annotations.SerializedName;
import com.ipssi.emoveproject.business.mapping.User;

@SuppressWarnings({
        "FieldCanBeLocal",
        "WeakerAccess",
})
public abstract class UserDTO<U extends User> extends DTO<U> {

    private final String emailAddress;

    @SerializedName("__type__")
    private final Type type;

    UserDTO(U user, Type type) {
        super(user);
        this.type = type;
        emailAddress = user.getEmailAddress();
    }

    protected enum Type {
        CUSTOMER,
        ADMIN
    }
}
