package com.ipssi.emoveproject.web.api.model.input;


import com.albema.spring.validation.UniqueEntityProperty;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.User;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@SuppressWarnings("unused")
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class AdminCreation implements UserService.AdminCreation, Serializable {


    @Email
    @NotEmpty
    @UniqueEntityProperty(
            entityClass    = User.class,
            entityProperty = "emailAddress"
    )
    protected String emailAddress;

    @NotBlank
    protected String password;
}
