package com.ipssi.emoveproject.web.common.helper;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;

import static java.util.Calendar.*;
import static javax.mail.Message.RecipientType.TO;
import static org.springframework.ui.freemarker.FreeMarkerTemplateUtils.*;

@Component
public class HtmlMailer {

    private final static String TEMPLATE_EXTENSION;

    static {
        TEMPLATE_EXTENSION = "ftl";
    }

    private final String        mailFrom;

    private final JavaMailSender mailSender;

    private final Configuration configuration;

    public void send(
            String         templateName,
            String         subject,
            String         to,
            Map<String, ?> model
    ) throws IOException, TemplateException, MessagingException {
        final Template    template;
        final String      content;
        final MimeMessage message;

        template = configuration.getTemplate(
                templateName + '.' + TEMPLATE_EXTENSION
        );

        content = processTemplateIntoString(template, model);

        message = mailSender.createMimeMessage();

        message.setSentDate(getInstance().getTime());
        message.setSubject(subject);
        message.setRecipient(TO, new InternetAddress(to));
        message.setFrom(mailFrom);
        message.setContent(content, "text/html");

        mailSender.send(message);
    }

    public <T> void send(
            String                           templateName,
            String                           subject,
            Function<T, String>              lazyTo,
            Map<String, Function<T, Object>> lazyModel,
            List<T>                          populators
    ) throws IOException, TemplateException, MessagingException {
        final Template              template;
        final List<MimeMessage>     messages;
        final Map<String, Object>   model;
        final Date                  now;

        MimeMessage message;

        template = configuration.getTemplate(
                templateName + '.' + TEMPLATE_EXTENSION
        );

        messages = new ArrayList<>(populators.size());

        model = new HashMap<>(lazyModel.size());

        now = getInstance().getTime();

        for(final T populator : populators) {
            lazyModel.forEach((k, v) -> model.put(k, v.apply(populator)));

            message = mailSender.createMimeMessage();

            message.setSentDate(now);
            message.setSubject(subject);
            message.setRecipient(TO, new InternetAddress(lazyTo.apply(populator)));
            message.setFrom(mailFrom);
            message.setContent(processTemplateIntoString(template, model), "text/html");

            messages.add(message);
        }

        mailSender.send(
                messages.toArray(
                        new MimeMessage[0]
                )
        );
    }

    HtmlMailer(
            @Value("${mail.from}") String         mailFrom,
                                   JavaMailSender mailSender,
                                   Configuration  configuration
    ) {
        this.mailFrom      = mailFrom;
        this.mailSender    = mailSender;
        this.configuration = configuration;
    }
}
