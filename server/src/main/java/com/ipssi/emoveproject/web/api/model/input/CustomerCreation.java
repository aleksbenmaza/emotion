package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.UniqueEntityProperty;
import com.fatboyindustrial.gsonjavatime.LocalDateConverter;
import com.google.gson.annotations.JsonAdapter;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.User;
import com.ipssi.emoveproject.web.api.model.input.validation.Majority;
import lombok.*;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class CustomerCreation implements UserService.CustomerCreation, Serializable {

    @Email
    @NotEmpty
    @UniqueEntityProperty(
            entityClass    = User.class,
            entityProperty = "emailAddress"
    )
    protected String emailAddress;

    @NotBlank
    protected String password;

    @NotBlank
    private String lastName;

    @NotBlank
    private String firstName;

    @NotNull
    @Majority
    @JsonAdapter(LocalDateConverter.class)
    private LocalDate birthDate;

    @NotBlank
    private String address;

    @NotBlank
    private String city;

    @NotEmpty
    @Pattern(regexp = "^\\d{5}$")
    private String zipCode;
}
