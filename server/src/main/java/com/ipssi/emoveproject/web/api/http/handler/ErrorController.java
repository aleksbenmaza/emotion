package com.ipssi.emoveproject.web.api.http.handler;

import com.albema.common.http.error.CustomHttpExceptions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpStatus.valueOf;

@RestController
class ErrorController {

    @RequestMapping("/errors")
    public void onRequest(
            final HttpServletResponse response
    ) {
        if (valueOf(response.getStatus()).is2xxSuccessful())
            throw new CustomHttpExceptions.ResourceNotFoundException();
    }
}
