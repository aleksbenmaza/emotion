package com.ipssi.emoveproject.web.api.model.input;

import com.albema.spring.validation.UniqueEntityProperty;
import com.ipssi.emoveproject.business.logic.VehicleService;
import com.ipssi.emoveproject.business.mapping.Make;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class MakeCreation implements VehicleService.MakeCreation, Serializable {

    @NotNull
    @Length(min = 1, max = 4)
    @UniqueEntityProperty(
            entityClass = Make.class,
            entityProperty = "code"
    )
    private String code;

    private String name;
}
