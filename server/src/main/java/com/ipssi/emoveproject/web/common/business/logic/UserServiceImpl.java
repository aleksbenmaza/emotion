package com.ipssi.emoveproject.web.common.business.logic;

import com.albema.common.util.Hasher;
import com.ipssi.emoveproject.business.logic.UserService;
import com.ipssi.emoveproject.business.mapping.Admin;
import com.ipssi.emoveproject.business.mapping.Customer;
import com.ipssi.emoveproject.business.mapping.User;
import com.ipssi.emoveproject.business.repository.UserDAO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.albema.common.util.Hasher.Algorithm.SHA_512;

@Service
class UserServiceImpl implements UserService {

    private final String hashSalt;

    private final UserDAO userDAO;

    @Override
    @Transactional(readOnly = true)
    public User getOne(BigInteger id) {
        return userDAO.find(id);
    }

    @Override
    @Transactional(readOnly = true)
    public User getOne(String emailAddress) {
        return userDAO.findByEmailAddress(emailAddress);
    }

    @Override
    @Transactional(readOnly = true)
    public User getOne(String emailAddress, String password) throws NoSuchAlgorithmException {
        return userDAO.findByEmailAddressAndHash(
                emailAddress,
                hash(emailAddress, password)
        );
    }

    @Override
    @Transactional
    public void delete(BigInteger... ids) {
        userDAO.remove(ids);
    }

    @Override
    @Transactional
    public void update(BigInteger id, String newPassword) throws NoSuchAlgorithmException {
        final User user;
        final String hash;

        user = userDAO.find(id);

        hash = hash(user.getEmailAddress(), newPassword);

        user.setHash(hash);

        userDAO.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Customer> getCustomers(BigInteger... ids) {
        return userDAO.find(Customer.class, ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Customer> getAllCustomers() {
        return userDAO.find(Customer.class);
    }

    @Override
    @Transactional(readOnly = true)
    public Customer getOneCustomer(BigInteger id) {
        return userDAO.find(Customer.class, id);
    }

    @Override
    @Transactional
    public Customer createCustomer(CustomerCreation creation) throws NoSuchAlgorithmException {
        final Customer customer;

        customer = new Customer(creation.getFirstName(), creation.getLastName(),
                creation.getBirthDate(), creation.getAddress(), creation.getCity(),
                creation.getZipCode(), creation.getEmailAddress());

        customer.setFirstName(creation.getFirstName());
        customer.setLastName(creation.getLastName());
        customer.setBirthDate(creation.getBirthDate());
        customer.setAddress(creation.getAddress());
        customer.setCity(creation.getCity());
        customer.setZipCode(creation.getZipCode());
        customer.setHash(hash(creation.getEmailAddress(), creation.getPassword()));

        userDAO.save(customer);

        return customer;
    }

    @Override
    @Transactional
    public void updateCustomer(BigInteger id, boolean mailValidated) {
        final Customer customer;

        customer = getOneCustomer(id);

        customer.setMailValidated(true);

        userDAO.save(customer);
    }


    @Override
    @Transactional
    public void updateCustomer(BigInteger id, CustomerUpdate customerUpdate) {
        final Customer customer;

        customer = userDAO.find(Customer.class, id);

        if(customer == null)
            return;

        if(customerUpdate instanceof GDPRUpdate)
            setCustomerByGDPR(customer);
        else if(customerUpdate instanceof CustomerDetailsUpdate)
            setCustomerByDetails(customer, (CustomerDetailsUpdate) customerUpdate);

        userDAO.save(customer);
    }

    private void setCustomerByGDPR(Customer customer) {
        customer.setLastName(null);
        customer.setFirstName(null);
        customer.setBirthDate(null);
        customer.setAddress(null);
        customer.setCity(null);
        customer.setZipCode(null);
        customer.setHash(null);
    }

    private void setCustomerByDetails(
            Customer              customer,
            CustomerDetailsUpdate update
    ) {
        customer.setAddress(update.getAddress());
        customer.setCity(update.getCity());
        customer.setZipCode(update.getZipCode());
    }

    String hash(String emailAddress, String password) throws NoSuchAlgorithmException {
        return Hasher.hash(emailAddress + hashSalt + password, SHA_512);
    }

    UserServiceImpl(
            @Value("${hashSalt}") String hashSalt,
            UserDAO userDAO
    ) {
        this.hashSalt = hashSalt;
        this.userDAO = userDAO;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Admin> getAdmins(BigInteger... ids) {
        return userDAO.find(Admin.class, ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Admin> getAllAdmins() {
        return userDAO.find(Admin.class);
    }

    @Override
    @Transactional(readOnly = true)
    public Admin getOneAdmin(BigInteger id) {
        return userDAO.find(Admin.class, id);
    }

    @Override
    @Transactional
    public Admin createAdmin(AdminCreation creation) throws NoSuchAlgorithmException {
        final Admin admin;

        admin = new Admin(
                creation.getEmailAddress(),
                hash(
                        creation.getEmailAddress(),
                        creation.getPassword()
                )
        );

        userDAO.save(admin);

        return admin;
    }
}
