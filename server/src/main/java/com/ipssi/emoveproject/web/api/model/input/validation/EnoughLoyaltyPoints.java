package com.ipssi.emoveproject.web.api.model.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

import static java.time.temporal.ChronoUnit.DAYS;

@Constraint(validatedBy = EnoughLoyaltyPointsValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnoughLoyaltyPoints {

    String durationAttribute() default "duration";

    String customerIdAttribute() default "customerId";

    String vehicleIdAttribute() default "vehicleId";

    ChronoUnit chronoUnit() default DAYS;

    String message() default "EnoughLoyaltyPoints";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
