package com.ipssi.emoveproject.web.api.model.output;

import com.fatboyindustrial.gsonjavatime.LocalDateConverter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.ipssi.emoveproject.business.mapping.Vehicle;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

@SuppressWarnings({
        "unused",
        "FieldCanBeLocal",
})
public class VehicleDTO extends DTO<Vehicle> {

    private final String color;

    private final String serialNumber;

    private final String plateNumber;

    private final BigDecimal numberOfKilometers;

    @JsonAdapter(LocalDateConverter.class)
    private final LocalDate purchaseDate;

    private final Float purchasePrice;

    private final Float rentalPrice;

    private final Integer loyaltyPoints;

    private final boolean hidden;

    @SerializedName("model")
    private final ModelDTO modelDTO;

    private final BigInteger[] rentalIds;

    public VehicleDTO(Vehicle vehicle) {
        super(vehicle);

        color = vehicle.getColor();
        serialNumber = vehicle.getSerialNumber();
        plateNumber = vehicle.getPlateNumber();
        numberOfKilometers = vehicle.getNumberOfKilometers();
        purchaseDate = vehicle.getPurchaseDate();
        purchasePrice = vehicle.getPurchasePrice();
        rentalPrice = vehicle.getRentalPrice();
        loyaltyPoints = vehicle.getLoyaltyPoints();
        hidden = vehicle.isHidden();

        modelDTO = new ModelDTO(vehicle.getModel());

        rentalIds = getIds(vehicle.getRentals());
    }
}
