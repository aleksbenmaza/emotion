package com.ipssi.emoveproject.business.mapping;

public enum VehicleType {
    CAR, SCOOTER
}
