package com.ipssi.emoveproject.business.mapping;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "customers")
@SecondaryTable(
        name          = "customer_loyalty_points",
        pkJoinColumns = @PrimaryKeyJoinColumn(
                name                 = "id",
                referencedColumnName = "id"
        )
)
@NoArgsConstructor(access = PROTECTED)
public class Customer extends User {

    @Getter
    @Setter
    @Column(name = "last_name")
    private String lastName;

    @Getter
    @Setter
    @Column(name = "first_name")
    private String firstName;

    @Getter
    @Setter
    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Getter
    @Setter
    private String address;

    @Getter
    @Setter
    private String city;

    @Getter
    @Setter
    @Column(name = "zip_code")
    private String zipCode;

    @Getter
    @Setter
    @Column(name = "mail_validated")
    private boolean mailValidated;

    @Getter
    @Column(
            table      = "customer_loyalty_points",
            name       = "value",
            insertable = false,
            updatable  = false
    )
    private Integer loyaltyPoints;

    @OneToMany(
            mappedBy = "customer",
            cascade = ALL,
            orphanRemoval = true
    )
    private Set<Rental> rentals;

    {
        rentals = new HashSet<>(0);
    }

    public Customer(@NonNull String lastName, @NonNull String firstName, @NonNull LocalDate birthDate,
                    @NonNull String address, @NonNull String city, @NonNull String zipCode,
                    @NonNull String emailAddress) {
        super(emailAddress);

        this.lastName = Validate.notBlank(lastName, "Last name cannot be blank");
        this.firstName = Validate.notBlank(firstName, "First name cannot be blank");
        this.birthDate = birthDate;
        this.address = Validate.notBlank(address, "Address cannot be blank");
        this.city = Validate.notBlank(city, "City cannot be bank");
        this.zipCode = zipCode;

        validateBirthDate(birthDate);
        validateZipCode(zipCode);
    }

    private void validateBirthDate(@NonNull LocalDate birthDate) {
        Validate.notNull(birthDate, "Birth date cannot be null", birthDate);

        LocalDateTime now = LocalDateTime.now();
        long age = now.getYear() - birthDate.getYear();

        Validate.isTrue(age > 18, "You need to have majority for rental vehicle");
    }

    private void validateZipCode(@NonNull String zipCode) {
        Validate.notBlank(zipCode, "Zip code cannot be blank");

        int countZipCode = zipCode.length();

        Validate.isTrue(countZipCode < 6, "Zip code cannot be superior to five character");
    }

    public Set<Rental> getRentals() {
        return new HashSet<>(rentals);
    }

    @SuppressWarnings("all")
    boolean addRental(@NonNull Rental rental) {
        return rentals.add(rental);
    }
}
