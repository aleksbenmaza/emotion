package com.ipssi.emoveproject.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.ipssi.emoveproject.business.mapping.User;

import java.math.BigInteger;
import java.util.List;

public interface UserDAO extends Finder<User>, Saver<User>, Remover<User> {

    User findByEmailAddress(String emailAddress);

    User findByEmailAddressAndHash(String emailAddress, String hash);

    <U extends User> List<U> find(Class<U> userClass);

    <U extends User> U find(Class<U> userClass, BigInteger id);

    <U extends User> List<U> find(Class<U> userClass, BigInteger[] ids);
}
