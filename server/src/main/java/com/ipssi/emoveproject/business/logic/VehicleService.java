package com.ipssi.emoveproject.business.logic;

import com.ipssi.emoveproject.business.mapping.Make;
import com.ipssi.emoveproject.business.mapping.Model;
import com.ipssi.emoveproject.business.mapping.Vehicle;
import com.ipssi.emoveproject.business.mapping.VehicleType;
import com.ipssi.emoveproject.business.repository.VehicleDAO.Search;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface VehicleService {

    List<Vehicle> get(BigInteger... ids);

    List<Vehicle> getAll();

    Vehicle getOne(BigInteger id);

    Vehicle create(Creation creation);

    void delete(BigInteger... ids);

    List<Model> getModels(BigInteger makeId, String name);

    List<Model> getModels(BigInteger makeId);

    List<Model> getModels(String name);

    List<Model> getModels(BigInteger... ids);

    List<Model> getAllModels();

    Model getOneModel(BigInteger id);

    Model createModel(ModelCreation creation);

    void deleteModels(BigInteger... ids);

    List<Make> getMakes(BigInteger... ids);

    List<Make> getMakes(String name);

    List<Make> getAllMakes();

    Make getOneMake(BigInteger id);

    Make createMake(MakeCreation creation);

    void deleteMakes(BigInteger... ids);

    List<Vehicle> get(Search search);

    List<Model> getModels(VehicleType vehicleType);

    List<Make> getMakes(VehicleType vehicleType);

    void update(BigInteger id, Update update);

    void updateMake(BigInteger id, String newName);

    void updateModel(BigInteger id, String newName);

    interface Creation {

        BigInteger getModelId();

        String getSerialNumber();

        String getColor();

        String getPlateNumber();

        BigDecimal getNumberOfKilometers();

        LocalDate getPurchaseDate();

        Float getPurchasePrice();

        Float getRentalPrice();

        Integer getLoyaltyPoints();
    }

    interface ModelCreation {

        BigInteger getMakeId();

        String getCode();

        VehicleType getVehicleType();

        String getName();
    }

    interface MakeCreation {

        String getCode();

        String getName();
    }

    interface Update {}

    interface HiddenUpdate extends Update {

        boolean isHidden();
    }

    interface KilometerageUpdate extends Update {

        BigDecimal getKilometerage();
    }

    interface RentalPriceAndLoyaltyPointsUpdate extends Update {

        Float getRentalPrice();

        Integer getLoyaltyPoints();
    }
}
