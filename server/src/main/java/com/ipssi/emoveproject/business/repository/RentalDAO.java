package com.ipssi.emoveproject.business.repository;

import com.albema.common.orm.repository.Finder;
import com.albema.common.orm.repository.Remover;
import com.albema.common.orm.repository.Saver;
import com.ipssi.emoveproject.business.mapping.Rental;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface RentalDAO extends Finder<Rental>, Saver<Rental>, Remover<Rental> {

    List<Rental> findByCustomerId(BigInteger id);

    Rental findByCustomerId(BigInteger rentalId, BigInteger customerId);

    List<Rental> find(
            LocalDate maxExclusiveDropOffDate,
            Integer   overdueDuration
    );
}
