package com.ipssi.emoveproject.business.mapping;

import com.albema.common.orm.entity.RecordableEntity;
import com.albema.common.orm.entity.identifiable.IdentifiableById;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "vehicles")
@NoArgsConstructor(access = PROTECTED)
@EqualsAndHashCode(of = "serialNumber", callSuper = false)
public class Vehicle extends RecordableEntity implements IdentifiableById {

    @Getter
    @Id
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "vehicleSeqGen"
    )
    @SequenceGenerator(
            name = "vehicleSeqGen",
            sequenceName = "seq_vehicles"
    )
    private BigInteger id;

    @Getter
    @ManyToOne(cascade = ALL, optional = false)
    @JoinColumn(
            name = "model_id",
            referencedColumnName = "id",
            nullable = false
    )
    private Model model;

    @Getter
    @Basic(optional = false)
    @Column(name = "serial_number")
    private String serialNumber;

    @Getter
    @Setter
    private String color;

    @Getter
    @Setter
    @Column(name = "plate_number")
    private String plateNumber;

    @Getter
    @Setter
    @Column(name = "km_number")
    private BigDecimal numberOfKilometers;

    @Getter
    @Setter
    @Column(name = "purchase_date")
    private LocalDate purchaseDate;

    @Getter
    @Setter
    @Column(name = "purchase_price")
    private Float purchasePrice;

    @Getter
    @Setter
    @Column(name = "rental_price")
    private Float rentalPrice;

    @Getter
    @Setter
    @Column(name = "loyalty_points")
    private Integer loyaltyPoints;

    @Getter
    @Setter
    private boolean hidden;

    @OneToMany(
            mappedBy = "vehicle",
            cascade = ALL,
            orphanRemoval = true
    )
    private Set<Rental> rentals;

    {
        rentals = new HashSet<>(0);
    }

    public Vehicle(
            @NonNull String serialNumber,
            @NonNull Model model
    ) {
        this.model = requireNonNull(model, "Model cannot be null");
        this.serialNumber = Validate.notBlank(serialNumber, "Serial number cannot be null");

        model.addVehicle(this);
    }

    public Set<Rental> getRentals() {
        return new HashSet<>(rentals);
    }

    boolean addRental(@lombok.NonNull Rental rental) {
        return rentals.add(rental);
    }
}
