<#import "macros.ftl" as m/>
<h1>
    <@m.message code="overdueDuration.introduction"
                vars=[customerFirstName,customerLastName]/>
</h1>
<p>
    <@m.message code="overdueDuration.mainMessage"
                vars=[rentalOverdueDuration,makeName,modelName]/>
</p>
<footer>
    ---------------
    <em><b><@m.message code="common.mailFooter"/></b></em>
</footer>