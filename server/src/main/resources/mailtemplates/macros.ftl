<#macro message code vars=[]>
  ${messageSource.getMessage(code, vars, locale)}
</#macro>