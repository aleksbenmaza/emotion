<#import "macros.ftl" as m/>
<h1>
    <@m.message code="emailValidation.introduction" vars=[firstName,lastName]/>
</h1>
<@m.message code="emailValidation.instruction"/>
<form method="post" action="http://${validationServer}/${customerId}" target="_blank">
    <input value="${validationCode}" hidden>
    <input value="${emailAddress}" hidden>
    <button type="submit"><@m.message code="emailValidation.submitButtonText"/></button>
</form>
<p>
    ---------------
    <em><b><@m.message code="common.mailFooter"/></b></em>
</p>