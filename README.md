LICENSE
-------

Please, consider reading the [GPL license agreement](LICENSE.txt).

REQUIREMENTS
------------

- Java Development Kit 8
- Maven
- npm
- Docker (optional)

To build the server project you must have [Maven](https://maven.apache.org/download.cgi) installed on your computer. Run `mvn package` from project root.
If Docker and Docker Compose are installed on your computer, run `docker-compose up`, it will start Tomcat container on `localhost:8888`, otherwise pick `emotion-project.war`, rename it to `ROOT.war` and place it into your application server's `webapps` folder.

To build the client side project, the same goes for [npm](https://www.npmjs.com/get-npm), run `npm install` then `ng serve`, you will want to open your favourite web browser on `localhost:5400`.

JAVADOC
-------

Read the [server project's API](https://desiroux.gitlab.io/emotion/).

_P.S._
------

_Keep in mind this project is still a WIP, errors, bugs, non-finished things are present._


